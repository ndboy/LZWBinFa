package speed;

import java.io.PrintStream;



public class LZWBinfa{

	/*public void kiir(StringBuffer sb){
		melyseg = 0;
		kiir(gyoker, new PrintStream(System.out), sb);

	}*/

	private Csomopont fa;
	protected Csomopont gyoker;

	public LZWBinfa(){
		gyoker = new Csomopont();
		fa = gyoker;
	}

	public void build(char b){
		if(b == '0'){
			if(fa.nullasGyermek() == null){
				fa.ujNullasGyermek(new Csomopont('0'));
				fa = gyoker;
			}else{
				fa = fa.nullasGyermek();
			}
		}else{
			if(fa.egyesGyermek() == null){
				fa.ujEgyesGyermek(new Csomopont('1'));
				fa = gyoker;
			}else{
				fa = fa.egyesGyermek();
			}
		}
	}

}
