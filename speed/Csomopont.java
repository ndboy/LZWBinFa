package speed;

public class Csomopont{

	private char b;
	private Csomopont balNulla;
	private Csomopont jobbEgy;

	public Csomopont(char b){
		this.b = b;
	}

	public Csomopont(){
		this.b = '/';
	}

	public Csomopont nullasGyermek (){
		return balNulla;
	}

	public Csomopont egyesGyermek(){
		return jobbEgy;
	}

	public void ujNullasGyermek(Csomopont cs){
		balNulla = cs;
	}

	public void ujEgyesGyermek(Csomopont cs){
		jobbEgy = cs;
	}

	public char getBetu(){
		return b;
	}
}
