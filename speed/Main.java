package speed;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import threads.OutputFile;
import threads.Comput;

public class Main{

	public final static int bufferSize = 2048;

	public static void main(String[] args){

		int melyseg;
		double atlag, szoras;

		LZWBinfa binFa = new LZWBinfa();

		if(args.length != 2){
			System.out.println("Usage: java Main inFile outFile.");
			System.exit(-1);
		}


		try{
			
			FileReader input = new FileReader(args[0]);

			char cbuf[] = new char[128];
			boolean inComment = false;
			System.out.println("Beolvasás...");


				while((input.read(cbuf,0,cbuf.length)) != -1){

					for(int i = 0;i < cbuf.length;i++){

							char c = cbuf[i];

							if(c == 0x3e){
								
								inComment = true;
								continue;
							}

							if(c == 0xa){
								inComment = false;
								continue;
							}

							if(c == 0x4e)
								continue;

							if(inComment)
								continue;

							for(int j = 0;j < 8; j++){
								if((c & 0x80) == 0){
									
									binFa.build('0');
								}
								else{
									binFa.build('1');
								}
								c <<= 1;
						}
					}
				
			}
			input.close();
		}catch(IOException e){
			System.out.println("There's no such file as: '" + args[0] + "'.");
			System.exit(-2);

		}

		try{
			System.out.println("Számítások...");

			Comput co = new Comput(binFa.gyoker);
			Thread t1 = new Thread(co);
			t1.start();

			System.out.println("Kiíratás...");

			PrintStream output = new PrintStream(args[1]);
			StringBuffer sb = new StringBuffer(bufferSize);

			OutputFile of = new OutputFile(binFa.gyoker,output,sb);
			Thread t2 = new Thread(of);
			t2.start();

			t1.join();
			t2.join();

			melyseg = co.fMelyseg;
			szoras = co.fSzoras;
			atlag = co.fAtlag;

			output.println(sb.toString());
			output.println("depth: " + melyseg);
			output.println("mean: " + atlag);
			output.println("var: " + szoras);

			System.out.println("depth: " + melyseg);
			System.out.println("mean: " + atlag);
			System.out.println("var: " + szoras);

			
			output.close();
		}catch(IOException e){
			System.out.println("Output file cannot be created.");
			System.exit(-3);
		}catch(InterruptedException e){

		}
	}
}
