package threads;

import speed.Csomopont;
import java.io.PrintStream;
import speed.Main;

public class OutputFile implements Runnable{

	private int melyseg;
	private Csomopont gyoker;
	private PrintStream ps;
	private StringBuffer sb;

	public OutputFile(Csomopont gyoker, PrintStream ps, StringBuffer sb){
		this.gyoker = gyoker;
		this.ps = ps;
		this.sb = sb;
	}

	public void kiir(PrintStream ps, StringBuffer sb){
		melyseg = 0;
		kiir(gyoker,ps, sb);
	}


	public void kiir(Csomopont cs,PrintStream ps, StringBuffer sb){

		
			if(cs != null){
				
				++melyseg;
				kiir(cs.egyesGyermek(),ps, sb);
				
				for(int i = 0;i < melyseg;i++){

					if(Main.bufferSize <= sb.length() + 1){
						ps.print(sb.toString());
						sb.delete(0,sb.length());
						
					}
					sb.insert(sb.length(),"-");
				}
				if(Main.bufferSize <= sb.length() + (new String(cs.getBetu() + "(" + (melyseg-1) + ")\n" ).length())){
						ps.print(sb.toString());
						sb.delete(0,sb.length());
						
					}
				sb.insert(sb.length(),cs.getBetu() + "(" + (melyseg-1) + ")\n" );
				kiir(cs.nullasGyermek(),ps, sb);

				--melyseg;
				
			}
	}

	public void run(){
		kiir(ps, sb);
	}
}