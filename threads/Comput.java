package threads;

import speed.Csomopont;

public class Comput implements Runnable{

	private Csomopont gyoker;
	private int fszom;

	public Comput(Csomopont gyoker){
		this.gyoker = gyoker;
	}

	private int melyseg, atlagosszeg, atlagdb;
	private double szorasosszeg;
	private int maxMelyseg;
	private double atlag, szoras;

	public int fMelyseg;
	public double fAtlag, fSzoras;

	public void run(){
		fMelyseg = getMelyseg();
		fAtlag = getAtlag();
		fSzoras = getSzoras();
	}

	protected int getMelyseg(){
		melyseg = maxMelyseg = 0;
		rmelyseg(gyoker);
		return maxMelyseg -1;
	}

	protected double getAtlag(){
		melyseg = atlagosszeg = atlagdb = 0;
		ratlag(gyoker);
		atlag = ((double) atlagosszeg) / atlagdb;
		return atlag;
	}

	protected double getSzoras(){
		atlag = getAtlag();
		szorasosszeg = 0.0;
		melyseg = atlagdb = 0;

		rszoras(gyoker);

		if(atlagdb -1 > 0)
			szoras = Math.sqrt(szorasosszeg / (atlagdb -1));
		else
			szoras = Math.sqrt(szorasosszeg);

		return szoras;
	}

	protected void rmelyseg(Csomopont cs){
		if(cs != null){
			melyseg++;
			
			rmelyseg(cs.egyesGyermek());
			rmelyseg(cs.nullasGyermek());
			if(melyseg > maxMelyseg)
				maxMelyseg = melyseg;
			melyseg--;
		}
	}

	protected void ratlag(Csomopont cs){
		if(cs != null){
			melyseg++;
			ratlag(cs.egyesGyermek());
			ratlag(cs.nullasGyermek());
			melyseg--;
			if(cs.egyesGyermek() == null && cs.nullasGyermek() == null){
				atlagdb++;
				atlagosszeg += melyseg;
			}
		}
	}

	protected void rszoras(Csomopont cs){
		if(cs != null){
			melyseg++;
			rszoras(cs.egyesGyermek());
			rszoras(cs.nullasGyermek());
			melyseg--;
			if(cs.egyesGyermek() == null && cs.nullasGyermek() == null){
				atlagdb++;
				szorasosszeg += ((melyseg -atlag)*(melyseg-atlag));
			}
		}
	}
}